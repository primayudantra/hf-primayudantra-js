import React, { Component } from 'react'
import { Link } from 'react-router'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SideBar from './components/__partials/sidebar'
import NavBar from './components/__partials/navbar'

class App extends Component {
  render(){
    return (
    <MuiThemeProvider>
      <div className="wrapper">
      	<SideBar/>
      	<div className="main-panel">
      		<NavBar/>
      		<div className="content">
      			{this.props.children}
      		</div>
      	</div>
      </div>
    </MuiThemeProvider>
    )
  }
}

export default App