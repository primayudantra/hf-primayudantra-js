import React, { Component } from 'react';

class VeggieOverview extends Component{
	render(){
		return(
			<div>
				{this.props.children}
			</div>
		)
	}
}
export default VeggieOverview