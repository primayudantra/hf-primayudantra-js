import React, { Component } from 'react';
import BaseView from '../__base/baseView'
import {observer} from 'mobx-react';
import menu from './menu.json'

@observer
class VeggieFragment extends BaseView{
	constructor(){
		super('veggie-box','current',menu);
	}
}
export default VeggieFragment