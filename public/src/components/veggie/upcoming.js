import React, { Component } from 'react';
import BaseView from '../__base/baseView'
import {observer} from 'mobx-react';
import menu from './menu.json'

@observer
class VeggieFragmentUpcoming extends BaseView{
	constructor(){
		super('veggie-box','upcoming',menu);
	}
}
export default VeggieFragmentUpcoming