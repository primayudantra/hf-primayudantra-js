import React, { Component } from 'react';
import BaseView from '../__base/baseView'
import {observer} from 'mobx-react';
import menu from './menu.json'

@observer
class VeggieFragmentPrevious extends BaseView{
	constructor(){
		super('veggie-box','previous',menu);
	}
}
export default VeggieFragmentPrevious