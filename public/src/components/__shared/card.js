import React, { Component } from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardHeader, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
const styleCard = {
  marginBottom:'20px'
}
class CardView extends Component{
	render(){
    var item = this.props.course;
		return(
      <Card 
            expanded={false}
            key={item.recipe.name}
            style={styleCard}
          >
            <CardHeader
            />
            <CardMedia
              overlay={<CardTitle title={item.recipe.name} subtitle={item.recipe.headline} />}
            >
              <img src={item.recipe.thumbLink} />
            </CardMedia>
            <CardText expandable={false}>
              <div className="row">
                <div className="col-md-8">
                  <b>Description:</b><br/>
                  {item.recipe.description} <br/>
                  <hr/>
                  <b>Allergens : </b>
                  {this.props.allergens.map((item,idx)=>{
                    return(
                      <span>{item.name}, </span>
                    )
                  })}
                  and etc
                </div>
                <div className="col-md-4">
                  <a href={item.recipe.cardLink} className="btn btn-fill btn-xs btn-success" target="_blank"><i className="fa fa-download" aria-hidden="true"></i> Download PDF</a><br/>
                  {item.recipe.averageRating == null ? 
                    <span>
                      <b>Average Rating : </b> - <br/>
                    </span>
                    :
                    <span>
                      <b>Average Rating : </b> {item.recipe.averageRating} <br/>
                    </span>
                  }
                  {item.recipe.ratingsCount == null ? 
                    <span>
                      <b>Rating Count : </b> - <br/>
                    </span>
                    :
                    <span>
                      <b>Rating Count : </b> {item.recipe.ratingsCount} <br/>
                    </span>
                  }
                  <b>Favorites Count:</b> {item.recipe.favoritesCount}<br/>
                  <b>Difficulty Level:</b> {item.recipe.difficulty}<br/>
                </div>
              </div>
              <div className="row">
                <hr/>
                <div className="col-md-6">
                Nutrition:
                  {this.props.nutrition.map((itemNut,idx) => {
                    return(
                      <div className='row' key={idx}>
                        <div className='col-md-6'>
                          {itemNut.name}
                        </div>
                        <div className='col-md-6'>
                          {itemNut.amount} {itemNut.unit}
                        </div>
                      </div>
                    )
                    })}
                </div>
                <div className="col-md-6">
                  Ingredients:
                    {this.props.ingredients.map((item,idx) => {
                      return(
                        <div className='row' key={idx}>
                          <div className='col-md-6'>
                            {item.name}
                          </div>
                        </div>
                      )
                    })}
                </div>
              </div>
            </CardText>
            <CardActions>
              <a href={item.recipe.websiteUrl} className="btn btn-success" target="_blank">
                <i className="fa fa-download" aria-hidden="true"></i> More Info
              </a>
            </CardActions>
          </Card>
		)
	}
}
export default CardView