import React, { Component } from 'react';
import BaseView from '../__base/baseView'
import {observer} from 'mobx-react';
import menu from './menu.json'

@observer
class ClassicFragment extends BaseView{
	constructor(){
		super('classic-box','current',menu);
	}
}
export default ClassicFragment