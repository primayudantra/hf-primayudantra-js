import React, { Component } from 'react';
import BaseView from '../__base/baseView'
import {observer} from 'mobx-react';
import menu from './menu.json'

@observer
class ClassicFragmentUpcoming extends BaseView{
	constructor(){
		super('classic-box','upcoming',menu);
	}
}
export default ClassicFragmentUpcoming