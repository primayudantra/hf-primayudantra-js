import React, { Component } from 'react';
import BaseView from '../__base/baseView'
import {observer} from 'mobx-react';
import menu from './menu.json'

@observer
class ClassicFragmentPrevious extends BaseView{
	constructor(){
		super('classic-box','previous',menu);
	}
}
export default ClassicFragmentPrevious