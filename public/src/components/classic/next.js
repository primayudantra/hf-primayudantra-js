import React, { Component } from 'react';
import BaseView from '../__base/baseView'
import {observer} from 'mobx-react';
import menu from './menu.json'

@observer
class ClassicFragmentNext extends BaseView{
	constructor(){
		super('classic-box','next',menu);
	}
}
export default ClassicFragmentNext