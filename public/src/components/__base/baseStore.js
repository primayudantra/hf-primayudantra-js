import { observable, action } from 'mobx';
import ajax from 'superagent';
import Config from '../../config/config.js'
class BaseStore{
	@observable data = []
	@observable courses = []
	@observable nutrition = []
	@observable ingredients = []
	@observable allergens = []
	@observable isLoading = false;

	@action get_lists(type,time){
		this.isLoading = true;
		ajax
		.get(Config.url.host+'/'+type+'/'+time)
    .end((err, response) => {
    	this.isLoading = false;
    	if(!err && response){
					this.data = response.body.items[0];
					this.courses = response.body.items[0].courses;
					this.nutrition = response.body.items[0].courses[0].recipe.nutrition;
					this.ingredients = response.body.items[0].courses[0].recipe.ingredients;
					this.allergens = response.body.items[0].courses[0].recipe.allergens;
				}else{
					console.log(err)
			}
    });
	}
}

export default new BaseStore();