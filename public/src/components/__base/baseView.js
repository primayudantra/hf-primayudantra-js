/** --------------------------------------------
Libs
-------------------------------------------- **/
import React, { Component } from 'react';
import {observer} from 'mobx-react';
import {Link} from 'react-router';
import store from './baseStore'

/** --------------------------------------------
Share Components
-------------------------------------------- **/

import CardView from './../__shared/card'
import IsLoading from '../__partials/loading';

/** --------------------------------------------
Material UI Components
-------------------------------------------- **/
import FlatButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';



const style = {
  display: 'inline-block',
  margin: '16px 32px 16px 0',
};
@observer
class BaseView extends Component{
	constructor(type,time,menu){
		super();
		this.store = store;
		this.type = type;
		this.time = time;
		this.menu = menu;
	}
	componentWillMount(){
		/** --------------------------------------------
		Calling ajax at baseStore
		-------------------------------------------- **/
		store.get_lists(this.type,this.time);
	}

	render(){
		if(store.isLoading === true){
			return( <IsLoading/> );
		}
		/** --------------------------------------------
		Card Components
		-------------------------------------------- **/
		var card = store.courses.map((item) => {
			return(
					<CardView 
					course={item} 
					nutrition={this.store.nutrition}
					ingredients={this.store.ingredients}
					allergens={this.store.allergens}
				/>
			)
		})
		/** --------------------------------------------
		Menu Components (import from menu.json)
		-------------------------------------------- **/
		var menu = this.menu.map((item,idx)=>{
			return(
				<Link to={item.path}><MenuItem primaryText={item.title} /></Link>
			)
		})
		return(
			<div>
				<div className="row">
					<div className="container">
						<div className="row">
							<div className="col-md-10">
								<h3>{store.data.headline} | Recipe <b>{this.time.toUpperCase()}</b> week</h3>
							</div>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-md-9">
					{card}
					</div>
					<div className="col-md-3">
						<Paper style={style}>
				      <Menu>
				      {menu}
				      </Menu>
				    </Paper>
					</div>
				</div>
			</div>
		)
	}
}
export default BaseView