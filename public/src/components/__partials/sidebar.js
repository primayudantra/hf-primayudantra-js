import React, { Component } from 'react';
import {Link} from 'react-router';


class SideBar extends Component{
    submitLogout(){
        window.location.href = '#/login';
        localStorage.clear();
    }
	render(){
		return(
            <div className="sidebar" data-color="green" data-image="assets/img/sidebar-5.jpg">
            	<div className="sidebar-wrapper">
                    <div className="logo">
                        <Link to="/" className="simple-text">
                            <img src="https://www.hellofresh.com/images/hellofresh-logo.svg?v=3" className="img-responsive"/>
                        </Link>
                    </div>

                    <ul className="nav">
                        <li>
                            <Link to="/classic-box">
                                <p>Classic Box</p>
                            </Link>
                        </li>
                        <li>
                            <Link to="/veggie-box">
                                <p>Veggie Box</p>
                            </Link>
                        </li>
                        <li className="active-pro" style={{cursor:'pointer'}}>
                            <a onClick={this.submitLogout.bind(this)}>
                                <i className="pe-7s-power"></i><p>Log Out</p>
                            </a>
                        </li>
                    </ul>
            	</div>
            </div>
		)
	}
}
export default SideBar