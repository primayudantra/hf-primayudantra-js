import React, { Component } from 'react';

class NavBar extends Component{
	render(){
		return(
        <nav className="navbar navbar-default navbar-fixed">
            <div className="container-fluid">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <a className="navbar-brand" href="https://www.hellofresh.com/tasty/food-boxes/">Our Plans</a>
                    <a className="navbar-brand" href="https://www.hellofresh.com/tasty/gift-landing/">Gift Card</a>
                    <a className="navbar-brand" href="https://blog.hellofresh.com/">Blog</a>
                </div>
            </div>
        </nav>
		)
	}
}
export default NavBar