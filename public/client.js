import React from 'react';
import ReactDOM from 'react-dom';

import { Router, hashHistory } from 'react-router';

import App from './src/App';

import LoginFragment from './src/components/login';

import HomeFragment from './src/components/home';

import VeggieFragment from './src/components/veggie';
import VeggieOverview from './src/components/veggie/overview';
import VeggieFragmentNext from './src/components/veggie/next';
import VeggieFragmentPrevious from './src/components/veggie/previous';
import VeggieFragmentUpcoming from './src/components/veggie/upcoming';

import ClassicFragment from './src/components/classic';
import ClassicOverview from './src/components/classic/overview';
import ClassicFragmentNext from './src/components/classic/next';
import ClassicFragmentPrevious from './src/components/classic/previous';
import ClassicFragmentUpcoming from './src/components/classic/upcoming';

import Auth from './src/services/auth';

let auth = new Auth();

const app = document.getElementById('app');
const routes = {
  Login : {
    path: '/login',
    component: LoginFragment
  },
  App : {
    path: '/',
    component: App,
    indexRoute: { component: HomeFragment },
    onEnter: auth.ensureAuth,
    childRoutes: [
			{ 
				path: '/veggie-box', 
        component:VeggieOverview,
        indexRoute: { component: VeggieFragment },
        childRoutes:[
          {
            path:'next',
            component:VeggieFragmentNext
          },
          {
            path:'upcoming',
            component:VeggieFragmentUpcoming
          },
          {
            path:'previous',
            component:VeggieFragmentPrevious
          },
        ]
  		},
  		{ 
				path: '/classic-box', 
				component:ClassicOverview,
        indexRoute: { component: ClassicFragment },
        childRoutes:[
          {
            path:'next',
            component:ClassicFragmentNext
          },
          {
            path:'upcoming',
            component:ClassicFragmentUpcoming
          },
          {
            path:'previous',
            component:ClassicFragmentPrevious
          },
        ]
  		},
  	],
  }
}

ReactDOM.render(
  <Router history={hashHistory} routes={[routes.Login, routes.App]} />
  ,app);
