## Flow
![Images](https://image.ibb.co/ePiCfa/hellofresh.png)
Backend(Node,Express,Pipa) and Frontend(React and Mobx)
## Steps
```
> $ npm install
> $ npm start
> open your browser and type localhost:8888

> localhost:8888/api/v1/classic-box/current <-- API for Current Classic Box
> localhost:8888/api/v1/classic-box/next <-- API for Next Recipe Classic Box
> localhost:8888/api/v1/classic-box/previous <-- API for Previous Classic Box
> localhost:8888/api/v1/classic-box/upcoming <-- API for Upcoming Classic Box

> localhost:8888/api/v1/veggie-box/current <-- API for Current Veggie Box
> localhost:8888/api/v1/veggie-box/next <-- API for Next Recipe Veggie Box
> localhost:8888/api/v1/veggie-box/previous <-- API for Previous Veggie Box
> localhost:8888/api/v1/veggie-box/upcoming <-- API for Upcoming Veggie Box
```
## Login Username and Password
```
Login : 
Username : primayudantra
Password : test
```
Boilerplate for Backend(Node,Express,Pipa) and Frontend(React x Mobx)

- primayudantra / [primayudantra.me](http://primayudantra.me) / @_prmydntr / prima.yudantra@gmail.com
