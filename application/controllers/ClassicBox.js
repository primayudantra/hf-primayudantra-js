let path = require('path'),
		request = require('superagent'),
		API 		= require('../../config/api.js')


let APIurl = API.default.url;
module.exports = {
	classicBoxWeek: function (req, res, next) {
		/** --------------------------------------------
	  *
	  *   GET API 
	  *   /api/v1/classic-box-week
	  *
	  -------------------------------------------- **/
		var data = {};
		request.get(APIurl.host+'?week=current&country=US&product=classic-box')
    .set({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + APIurl.token})
    .end(function(err, response) {
    	data = response.text;
    	var result = JSON.parse(data)
      res.send(result)
    });
  },

  /** --------------------------------------------
	  *
	  *   GET API 
	  *   /api/v1/classic-box-next
	  *
	  -------------------------------------------- **/
  classicBoxNext: function (req, res, next) {
		var data = {};
		request.get(APIurl.host+'?week=next&country=US&product=classic-box')
    .set({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + APIurl.token})
    .end(function(err, response) {
    	data = response.text;
    	var result = JSON.parse(data)
      res.send(result)
    });
  },

  /** --------------------------------------------
	  *
	  *   GET API 
	  *   /api/v1/classic-box-previous
	  *
	  -------------------------------------------- **/
  classicBoxPrevious: function (req, res, next) {
		var data = {};
		request.get(APIurl.host+'?week=previous&country=US&product=classic-box')
    .set({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + APIurl.token})
    .end(function(err, response) {
    	data = response.text;
    	var result = JSON.parse(data)
      res.send(result)
    });
  },

  /** --------------------------------------------
	  *
	  *   GET API 
	  *   /api/v1/classic-box-upcoming
	  *
	  -------------------------------------------- **/
  classicBoxUpcming: function (req, res, next) {
		var data = {};
		request.get(APIurl.host+'?week=upcoming&country=US&product=classic-box')
    .set({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + APIurl.token})
    .end(function(err, response) {
    	data = response.text;
    	var result = JSON.parse(data)
      res.send(result)
    });
  },
}