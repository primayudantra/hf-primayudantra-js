let path = require('path');

module.exports = {
	/** --------------------------------------------
	  *
	  *   Routing index
	  *   / (routing / index)
	  *
	  -------------------------------------------- **/
	index: function(req,res,next){
		res.sendFile(path.resolve('public/index.html'));
	},
}