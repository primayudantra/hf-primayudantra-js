let path = require('path'),
		request = require('superagent'),
		API 		= require('../../config/api.js')


let APIurl = API.default.url;
module.exports = {
  /** --------------------------------------------
  *
  *   GET API 
  *   /api/v1/veggie-box-week
  *
  -------------------------------------------- **/
	veggieBoxWeek: function (req, res, next) {
		var data = {};
		request.get(APIurl.host+'?week=current&country=US&product=veggie-box')
    .set({'Content-Type': 'application/json','charset':'UTF-8', 'Authorization': 'Bearer ' + APIurl.token})
    .end(function(err, response) {
    	data = response.text;
      var result = JSON.parse(data)
      res.send(result)
    });
  },

  /** --------------------------------------------
  *
  *   GET API 
  *   /api/v1/veggie-box-next
  *
  -------------------------------------------- **/
  veggieBoxNext: function (req, res, next) {
		var data = {};
		request.get(APIurl.host+'?week=next&country=US&product=veggie-box')
    .set({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + APIurl.token})
    .end(function(err, response) {
    	data = response.text;
      var result = JSON.parse(data)
      res.send(result)
    });
  },

  /** --------------------------------------------
  *
  *   GET API 
  *   /api/v1/veggie-box-previous
  *
  -------------------------------------------- **/
  veggieBoxPrevious: function (req, res, next) {
		var data = {};
		request.get(APIurl.host+'?week=previous&country=US&product=veggie-box')
    .set({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + APIurl.token})
    .end(function(err, response) {
    	data = response.text;
      var result = JSON.parse(data)
      res.send(result)
    });
  },

  /** --------------------------------------------
  *
  *   GET API 
  *   /api/v1/veggie-box-upcoming
  *
  -------------------------------------------- **/
  veggieBoxUpcoming: function (req, res, next) {
		var data = {};
		request.get(APIurl.host+'?week=upcoming&country=US&product=veggie-box')
    .set({'Content-Type': 'application/json', 'Authorization': 'Bearer ' + APIurl.token})
    .end(function(err, response) {
    	data = response.text;
      var result = JSON.parse(data)
      res.send(result)
    });
  },
}